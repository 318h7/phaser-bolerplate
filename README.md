# Phaser 3 Typescript Webpack Boilerplate

This is a boilerplate for starting new Phaser + Typescript projects

Bundle includes:

- Phaser 3
- Typescript 4
- Webpack 5
- Eslint 7
- Yarn 2 (no PnP)

## Setup

### Prerequisites 

- [NodeJS](https://nodejs.org/)
- [Yarn](https://yarnpkg.com/) (optional)

### 1. Clone this repo:

Navigate into your workspace directory.

Run:

`git clone git@gitlab.com:318h7/phaser-bolerplate.git`


### 3. Install dependencies:

```
cd phaser-boilerplate
yarn
```

### 4. Run the development server:

Run:

`yarn run dev`

This will run a server so you can run the game in a browser.

Open your browser and visit [localhost](localhost:3000).

Also this will start a watch process, so you can change the source and the process will recompile and refresh the browser


## Build for deployment:

Run:

`yarn run build`

This will optimize and minimize the compiled bundle.

## Credits

This work is based off of several existing repos:

https://github.com/nkholski/phaser3-es6-webpack

https://github.com/lean/phaser-es6-webpack

https://github.com/troyedwardsjr/phaser3-typescript-webpack
