const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin');

var definePlugin = new webpack.DefinePlugin({
  __DEV__: JSON.stringify(JSON.parse(process.env.BUILD_DEV || 'true')),
  WEBGL_RENDERER: true,
  CANVAS_RENDERER: true
})

module.exports = {
  mode: 'none',
  entry: {
    app: {
      import: './src/main.ts',
      dependOn: 'vendor',
    },
    vendor: 'phaser',
  },
  output: {
    filename: '[name].js',
  },
  plugins: [
    definePlugin,
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './static/index.html',
      chunksSortMode: 'manual',
      minify: {
          removeAttributeQuotes: false,
          collapseWhitespace: false,
          html5: false,
          minifyCSS: false,
          minifyJS: false,
          minifyURLs: false,
          removeComments: false,
          removeEmptyAttributes: false
      },
      hash: false
    }),
    new CopyPlugin({
      patterns: [
        { from: "./assets", to: "assets" },
      ],
    }),
  ],
  module: {
    rules: [{
      test: /\.ts$/,
      use: require.resolve('ts-loader'),
      include: path.join(__dirname, 'src'),
      exclude: /node_modules/,
    }]
  },
  resolve: {
    extensions: ['.ts', '.js'],
  }
}
