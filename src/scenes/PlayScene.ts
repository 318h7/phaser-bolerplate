import Phaser from 'phaser';

class TestScene extends Phaser.Scene {
  constructor() {
    super({ key: 'TestScene' });
  }
	
  preload(): void {
    this.load.image('logo', '/assets/phaser.png');
  }

  create(): void {
    const logo = this.add.sprite(
      this.cameras.main.centerX,
      this.cameras.main.centerY,
      'logo'
    );

    logo.setOrigin(0.5, 0.5);
    logo.setScale(0.5, 0.5);

    this.tweens.add({
      targets: [logo],
      scaleX: 1,
      scaleY: 1,
      duration: 2000,
      yoyo: true,
      repeat: -1,
      ease: Phaser.Math.Easing.Bounce.InOut,
    });
  }
}

export default TestScene;
