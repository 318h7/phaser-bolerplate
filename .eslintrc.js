module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: './tsconfig.json',
  },
  plugins: [
    '@typescript-eslint',
  ],
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
  ],
  "env": {
    "browser": true
  },
  rules: {
    "yoda": ["error", "never"],
    "camelcase": "error",
    "eol-last": ["error"],
    "indent": ["error", 2],
    "quotes": ["error", "single"],
    "max-len": ["error", { code: 80 }],
    "semi": ["error", "always"],
  },
};
